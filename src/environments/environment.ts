// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
	production: false,
	mapbox: {
    accessToken: 'pk.eyJ1IjoibmlraXRhYSIsImEiOiJjanQ5NDFhYWIwMDRrNDlwbTZ4ZmN5YmN3In0.UMUupXhaDu2iorCmE09pYA'
	},
	opencage: {
		apiKey: 'fc6e0f0d4a6b466aa1cb4e624e4f3c56'
	},
	api: {
		base_url: 'http://localhost:3000'
	}
};
