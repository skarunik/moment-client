export const environment = {
	production: true,
	mapbox: {
    accessToken: 'pk.eyJ1IjoibmlraXRhYSIsImEiOiJjanQ5NDFhYWIwMDRrNDlwbTZ4ZmN5YmN3In0.UMUupXhaDu2iorCmE09pYA'
	},
	opencage: {
		apiKey: 'fc6e0f0d4a6b466aa1cb4e624e4f3c56'
	},
	api: {
		base_url: 'http://134.209.251.166:3000'
	}
};
