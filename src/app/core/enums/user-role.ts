export enum UserRole {
	Administrátor,
	Dispečer,
	Technik,
	Magistrát,
	Uživatel
}