# Moment

New application for **Future s.r.o.**

## Our team

* __Mykhailo Veremchuk__ <veremmyk@fel.cvut.cz> - Projektový vedoucí
* __Marek Peřina__ <perinma2@fel.cvut.cz> - Business analytik
* __Nikita Shkarupa__ <skarunik@fel.cvut.cz> - Business analytik
* __Tatiana Kulikova__ <kuliktat@fel.cvut.cz> - Business analytik

## Development

Use `yarn start` to run application locally, open your browser on `http://localhost:4200/`.  
**Don't use npm** to avoid -lock files issue.
